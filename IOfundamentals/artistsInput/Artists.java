package IOfundamentals.artistsInput;

import java.io.*;
import java.util.Scanner;

//Class:
public class Artists {

    //Reads the values of artist.txt and prints them out to the console.
    public static void readDatabase() throws IOException {
        try {
            System.out.println("Current artists/bands...");
            BufferedReader reader = new BufferedReader(new FileReader("artists.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
            //Catches the exception of the file not being found, states its empty..
        } catch (FileNotFoundException e) {
            System.out.println("<Artist database is currently empty>");
        }
    }

    //-------------------------------------------------------------------------------------
    //Prompts the user for input, stores the input on exit.
    public static void inputDatabase() throws IOException {

        System.out.println("Add your favourite artist to the list! ");
    //Scanner and BufferedWriter used for input and reading.
        Scanner userInput = null;
        BufferedWriter writer = null;
        try {
            // Tries to append the string to a file, if not existing, creates "artists.txt"
            writer = new BufferedWriter(new FileWriter("artists.txt", true));
            userInput = new Scanner(System.in);

            // Feeds the input through scanner.
            String input = userInput.nextLine();
            String exit = "exit";

            // Loops, until "exit" is entered, multiple inputs available.
            while (!input.equals(exit))
            {
                // Each input is stored in the buffer, entered with newLine /n with each entry.
                writer.write(input);
                writer.newLine();
                input = userInput.nextLine();
            }
        }
        finally
        {
            //When "exit", closes both functions, flushing the content to the artists.txt.
            if (userInput != null)
                userInput.close();
            if (writer != null)
                writer.close();
        }
    }
}
//    ___________________________________________________________________________________






















//    public Artists(String filePath) {
//        this.filePath = filePath;
//    }
//
//    public void printArtists(){
//        //create Filereader obj
//        //create bufferedReader obj
//        //loop through until EOF.
//        //print artist to console
//
//        try (FileReader fileReader = new FileReader(filePath);
//             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
//            String line = "";
//            while ((line = bufferedReader.readLine()))
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getMessage());
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }






