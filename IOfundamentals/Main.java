package IOfundamentals;
import IOfundamentals.artistsInput.Artists;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Artists Database!");

        //Reads the current values stored within the txt file, if not, states its empty.
        Artists.readDatabase();

        //Prompt the user for input, Read user Input, followed by the input being added to txtfile on exit.
        Artists.inputDatabase();
         }
}



