# IOfundamentals-ArtistsDatabase

Testing out IO functions with JAVA, buffering input data and saving it to a textfile. 

## How it works: 
Simulating adding data to a file through user input via console. 
  
When the application starts it checks whether the designated file does exist, if not, it states the "database"  
being empty. If it does exist, it reads the current list within the txt.file.

The user is then prompted to enter their favourite artists/bands to the list. The user can enter multiple entries 
which are stored within the buffer. 

The storing/saving of the actual data is not fulfilled until the app is terminated by typing "exit" into the   
console window. If the app shutdowns before the "exit"-termination the data is lost.

*See comments within the code to see respective details.*

**About:**
  
An assignment with specific conditions on how and when the data is saved through the use of IO functions  
and scanner. 

